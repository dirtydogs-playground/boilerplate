// import express from "express";

// const app = express();

// app.get("/", (req, res) => {
//   res.json({ response: "hello world" });
// });

// app.listen(3000, ()=>console.log("server running"))

(function(){
  // *** dependencies *** //
  const express = require('express');
  const appConfig = require('./config/main.config');
  const routeConfig = require('./config/route.config');


  // *** express instance *** //
  const app = express();


  // *** config *** //
  appConfig.init(app, express);
  routeConfig.init(app);

  module.exports = app;
})();