import { Express } from "express"
(function (routeConfig) {
  "use strict";

  routeConfig.init = function (app:Express) {
    // *** routes *** //
    const indexRouter = require("../index/index.routes");

    app.use("/", indexRouter);
  };

})(module.exports);
