import express, { Application } from "express";

// *** load environment variables *** //
import { CONFIG } from "./config.const";

(function (appConfig) {
  const path = require("path");
  const logger = require("morgan");
  const cors = require("cors");
  const bodyParser = require("body-parser");
  const cookieParser = require("cookie-parser");
  const session = require("express-session");
  const flash = require("connect-flash");
  const moment = require("moment");
  const passport = require("passport");
  const _ = require("lodash");
  const ExtractJwt = require("passport-jwt").ExtractJwt;
  const JwtStrategy = require("passport-jwt").Strategy;

  appConfig.init = function (app: Application, express: any) {
    app.use(express.static(path.join(__dirname, "../public")));

    app.set("views", path.join(__dirname, "../views"));
    app.set("view engine", "ejs");

    app.use(logger("dev"));
    app.use(express.json());

    app.use(
      express.urlencoded({
        extended: true,
      })
    );
    app.use(cors());
    app.options("*", cors());
    app.use(
      bodyParser.urlencoded({
        extended: true,
      })
    );
    app.use(bodyParser.json());

    /** CROS ORIGIN CONFIG START */
    const allowCrossDomain = function (req: any, res: any, next: any) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header(
        "Access-Control-Allow-Methods",
        "GET,POST,DELETE,OPTIONS,PUT,PATCH"
      );
      res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
      res.header("Access-Control-Expose-Headers", "x-filename");
      next();
    };
    app.use(allowCrossDomain);
    /** CROS ORIGIN CONFIG END */

    /** COOKIE AND SESSION CONFIG START */
    app.use(cookieParser("secret"));
    app.use(
      session({
        secret: "valucart",
        proxy: true,
        resave: true,
        saveUninitialized: true,
      })
    );
    /** COOKIE AND SESSION CONFIG END */

    app.use(flash());
    app.use(function (req: any, res, next) {
      res.locals.flashdata = req.flash();
      res.locals.moment = moment;
      res.locals._ = _;
      res.locals.baseURL = req.headers.host;
      next();
    });

    app.use(passport.initialize());
    app.use(passport.session());
    passport.serializeUser(function (user: any, done: any) {
      done(null, user);
    });
    passport.deserializeUser(function (user: any, done: any) {
      done(null, user);
    });

    const opts: any = {};
    // Setup JWT options
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
    opts.secretOrKey = CONFIG.token_secret;
    opts.passReqToCallback = true;

    passport.use(
      new JwtStrategy(opts, function (req: any, jwtPayload: any, done: any) {
        //If the token has expiration, raise unauthorized
        // var expirationDate = new Date(jwtPayload.exp * 1000)
        // if(expirationDate < new Date()) {
        //   console.log(expirationDate)
        //   return done(null, false);
        // }
        // done(null, user)

        const now = moment().unix();

        // check if the user has access to url
        // TODO make wilcard and some otheer presets
        // if(!jwtPayload.usserAccessList.includes(req.originalUrl)) done("User dont have access to this page")

        // check if the token has expired
        if (now > jwtPayload.exp) done("Token has expired.");
        else done(null, jwtPayload);
      })
    );

    // passport.use(
    //   new GoogleStrategy(
    //     {
    //       clientID: "17374109161-re1b1o17afrjrs6954m8ruche3d55rb4.apps.googleusercontent.com",
    //       clientSecret: "PQoW9EbWApriy3ur5KS-fyyZ",
    //       callbackURL: "http://localhost:3000/auth/google/callback"
    //     },
    //     function(accessToken, refreshToken, profile, done) {
    //       var userData = {
    //         email: profile.emails[0].value,
    //         name: profile.displayName,
    //         token: accessToken
    //       };
    //       done(null, userData);
    //     }
    //   )
    // );
  };
})(module.exports);
