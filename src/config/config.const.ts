require('dotenv').config(); //instatiate environment variables

//Make this global to use all over the application
export const CONFIG:any = {}
CONFIG.token_secret = process.env.TOKEN_SECRET || 'secret';
